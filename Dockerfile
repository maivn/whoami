FROM golang:1.8

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

ENV PORT 8000
EXPOSE 8000

CMD ["app"]